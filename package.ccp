{
  description = "A little package to print some pretty messages",
  author = "schroffl",
  name = "pretty-print",
  version = "0.1.0",
  dependencies = {},
  main = "index.lua",
}