return {
	success = require('./lib/success.lua'),
	warning = require('./lib/warning.lua'),
	error = require('./lib/error.lua')
};
