return function(...)
  term.setTextColor(colors.green);
  
  write('[+] ');
  
  term.setTextColor(colors.white);
  
  print(unpack( arg ));  
end
