# pretty-print Demo Package

This is a demo package for [CCPM](https://github.com/schroffl/ccpm) as shown in [this guide](https://github.com/schroffl/ccpm/tree/master/client/QUICKSTART.md)